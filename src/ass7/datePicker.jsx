import React, { Component } from "react";
class DatePicker extends Component {
    state = {
        yearsArr: [],
        monthsArr: [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        ],
        daysArr: [],
        selectedYear: "noyear",
        selectedMonth: "nomonth",
        selectedDay: "noday",
    };

    getDaysInMonth = (month, year) => {
        var date = new Date(year, month, 1);
        var days = [];
        let start = 1;
        while (date.getMonth() === month) {
            days.push(start);
            start+=1
            date.setDate(date.getDate() + 1);
        }
        console.log("getDays==>>",days)
        return days;
    }

    handleChange = (e) => {
        console.log(e.currentTarget);
        let s1 = { ...this.state }
        const { currentTarget: input } = e;
        if (input.name == 'year') {
            s1.selectedYear = input.value
            s1.selectedMonth='nomonth'
        }
        if (input.name == 'month') {
            console.log('=====',s1.selectedYear)
            if(s1.selectedYear=="" || !s1.selectedYear || input.value=='nomonth'){
                alert('Select Year first!')
            }else{
                s1.selectedMonth = input.value
                s1.selectedDay='noday'
                let monthIndex= s1.monthsArr.findIndex((e)=>e==s1.selectedMonth)
                let daysArr= this.getDaysInMonth(monthIndex,s1.selectedYear)
                s1.daysArr=daysArr
            }
        }
        if (input.name == 'day') {
            if(s1.selectedMonth==''){
                alert('select month!')
            }else{
                s1.selectedDay=input.value
            }
        }
        this.setState(s1)
    }

    populateYears = () => {
        let s1 = { ...this.state }
        let finalYear = 2021
        for (let startYear = 1998; startYear <= finalYear; startYear++) {
            s1.yearsArr.push(startYear)
        }
        this.setState(s1)
    }
    componentDidMount() {
        this.populateYears()
    }
    render() {
        let { yearsArr, monthsArr, daysArr, selectedYear, selectedDay, selectedMonth } = this.state
        return <div className="row">
            <div className="row col-2 ">
                <select className="form-control"
                    name="year" value={selectedYear}
                    onChange={this.handleChange}
                    placeholder="Year"
                >
                    <option disabled  value="noyear"> Select the Year</option>
                    {yearsArr.map((c1) => (
                        <option >{c1}</option>
                    ))}
                </select>
            </div>

            <div className="row col-2 ">
                <select className="form-control"
                    name="month" value={selectedMonth}
                    onChange={this.handleChange}
                    placeholder="Month"
                    
                >
                    <option disabled  value="nomonth"> Select the Month</option>
                    {monthsArr.map((c1) => (
                        <option >{c1}</option>
                    ))}
                </select>
            </div>
            <div className="row col-2 ">
                <select className="form-control"
                    name="day" value={selectedDay}
                    onChange={this.handleChange}
                    placeholder="Day"
                    value={selectedDay}
                >
                    <option disabled  value="noday"> Select the Day</option>
                    {daysArr.map((c1) => (
                        <option >{c1}</option>
                    ))}
                </select>
            </div>

        </div>
    }
}
export default DatePicker;