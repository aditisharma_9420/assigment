import React, { Component } from "react";
class Input1 extends Component {
    state = {
      
        allProducts: this.props.allProducts,
        product: this.props.product,
        brands: [],
        errors: {},
        categories: ["Food", "Personal Care", "Apparel"],
        specials: ["Special Offer", "Limited stock"],
      
     
    };

    handleChange = (e) => {
        console.log(e.currentTarget);
        const { currentTarget: input } = e;
        let s1 = { ...this.state };
        if (input.type == 'text') {
            s1.product[input.name] = input.value
        }
        if (input.type == 'checkbox') {
            if (input.checked && input.value == 'Special Offer') {
                s1.product.specialOffer = true
            } else if (input.checked == false && input.value == 'Special Offer') {
                s1.product.specialOffer = false
            }
            if (input.checked && input.value == 'Limited stock') {
                s1.product.limitedStock = true
            } else if (input.checked == false && input.value == 'Limited stock') {
                s1.product.limitedStock = false
            }
        }
        if (input.type == 'radio') {
            if (input.checked) {
                console.log("categoryyy", input.value)
                let braArr = []
                s1.allProducts.map((e => {
                    if (e.category == input.value) {
                        let i = braArr.findIndex((e1) => {
                            return e1 == e.brand
                        })
                        if (i == -1) {
                            braArr.push(e.brand)
                        }
                    }
                }))
                s1.product.category = input.value
                s1.brands = braArr
            }
        }
        if (input.name == 'brand') {
            console.log('droppp==>', input.value)
            s1.product.brand = input.value
        }

   
   this.handleValidate(e);
    this.setState(s1);
};
handleSubmit=(e)=>{
    e.preventDefault();
    let errors = this.validateAll();
   if(this.isValid(errors)) this.props.onSubmit(this.state.product);
    //console.log("handle submit",this.state.person);

else {
    let s1={...this.state};
    s1.errors=errors;
    this.setState(s1);
}
};
handleValidate=(e)=>{
   let {currentTarget:input}=e;
   let s1={...this.state};
   switch(input.name) {
       case "code":
    s1.errors.code=this.validateCode(input.value);
       break;
       case "price":
        s1.errors.price=this.validatePrice(input.value);
       break;
      case " brand":
       s1.errors. brand=this.validateCat(input. brand);
      break;
  

default:
break;
   }
   this.setState(s1);
};
isValid =(errors)=>{
    let keys = Object.keys(errors);
   let count= keys.reduce((acc,curr) => (errors[curr] ? acc + 1:acc),0);
    return count===0;
};
validateAll = ()=>{
    let { code, price, category, brand, specialOffer, limitedStock, quantity}=this.state.product
    let errors={};
    errors.code=this.validateCode(code)
    errors.price=this.validatePrice(price)
    errors.brand=this.validateCat(brand)
   // errors.specialOffer=this.validateDes(specialOffer)
   // errors.limitedStock=this.validateCode(limitedStock)
   // errors.category=this.validateCat(category)
  
   // errors.quantity=this.validateDiscount(quantity)

    return errors;
}
validateCode=(code)=>
    !code
    ? "code must be entered"
    :"";
  


  
    validatePrice=(price)=>
    !price
    ?"price must be entered"
 
    :"";

    validateCat=(category)=>
    !category
    ? "Required"
  
    :"";
    


    render() {
        let { code, price, category, brand, specialOffer, limitedStock, quantity } = this.state.product
        const { categories, specials, brands,errors } = this.state;
        return (
            <div>
                 <div className="container">
                <h5>{this.props.edit ?"Edit details":"Enter Details of product"}</h5>
                <div className="form-group">
                    <label>Code</label>
                    <input type="text"
                   
                    className="form-control"
                    id="code"
                    name="code"
                    value={code}
                    placeholder=""
                    disabled={this.props.edit}
                    onChange={this.handleChange}
                    onBlur={this.handleValidate}/>
                {errors.code?(
                <span className="text-danger">{errors.code}
                </span>):("")}
                </div>
                
                <div className="form-group">
                    <label>Price</label>
                    <input type="text"
                    className="form-control"
                    id="price"
                    name="price"
                    value={price}
                    placeholder=""
                    onChange={this.handleChange}
                    onBlur={this.handleValidate}/>
                       {errors.price?(
                <span className="text-danger">{errors.price}
                </span>):("")}
                </div><br/>
                
                <b>Category</b><br />
                    {categories.map((d1) => (<div className="form-check form-check-inline">
                        <input type="text"
                            className="form-check-input"
                            type="radio"
                            name="category"
                            value={d1}
                            checked={category === d1}
                            onChange={this.handleChange} />
                        <label className="form-check-label">{d1}</label>
                    </div>))}<br />

                    <div className="form-group"><br />
                        <select className="form-control"
                            name="brand" value={brand}
                            onChange={this.handleChange}
                        >
                            <option disabled value=""> Select the brand</option>
                            {brands.map((c1) => (
                                <option >{c1}</option>

                            ))}
                        </select>
                        {errors.brand?(
                <span className="text-danger">{errors.brand}
                </span>):("")}
                    </div>

                    <label className="form-check-label  font-weight-bold">
                        <b> Choose Other info about the Product</b>
                    </label>
                    {specials.map((t1) => (
                        <div className="form-check">
                            <input type="checkbox"
                                className="form-check-input"
                                checked={t1 == 'Special Offer' ? specialOffer : t1 == 'Limited stock' ? limitedStock : false}
                                name="special"
                                value={t1}
                                onChange={this.handleChange} />
                            <label className="form-check-label">{t1}</label>
                        </div>
                    ))}<br />
              
              </div><br />
       <button className="btn btn-primary" onClick={this.handleSubmit} >{this.props.edit?"update":"Submit"}</button>
               </div>
                  
                 
        )
    }
}
export default Input1;