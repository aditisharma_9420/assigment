import React, { Component } from "react";
import Input1 from "./input2"
import Recive from "./receive"
class MainComponent extends Component {
    state = {
        products: [
            {
                code: "PEP1253", price: 20, brand: "Pepsi", category: "Food",
                specialOffer: false, limitedStock: false, quantity: 25
            },
            {
                code: "MAGG021", price: 25, brand: "Nestle", category: "Food",
                specialOffer: true, limitedStock: true, quantity: 10
            },
            {
                code: "LEV501", price: 1000, brand: "Levis", category: "Apparel",
                specialOffer: true, limitedStock: true, quantity: 3
            },
            {
                code: "CLG281", price: 60, brand: "Colgate", category: "Personal Care",
                specialOffer: true, limitedStock: true, quantity: 5
            },
            {
                code: "MAGG451", price: 25, brand: "Nestle", category: "Food",
                specialOffer: true, limitedStock: true, quantity: 0
            },
            {
                code: "PAR250", price: 40, brand: "Parachute", category: "Personal Care",
                specialOffer: true, limitedStock: true, quantity: 5
            }
        ],

       
        view: 0,

        editPersonIndex: -1,
        viewCourseIndex: -1,

    }
    handlePerson = (product) => {
        console.log("In Handle Person", product);
        let s1 = { ...this.state };
        s1.editPersonIndex >= 0 ? (s1.products[s1.editPersonIndex] = product) :
            s1.products.push(product);
        s1.view = 0
        s1.editPersonIndex = -1;

        this.setState(s1);
    }
    handleStocks=(stockCode,stockQuantity)=>{
        console.log("In stock", stockCode,stockQuantity);
        let s1 = { ...this.state };
        let index=s1.products.findIndex((e)=>{
            return e.code==stockCode
        })
        s1.view=3
        s1.products[index].quantity+=parseInt(stockQuantity)
        this.setState(s1)
    }
    showForm = () => {
        let s1 = { ...this.state };
        s1.view = 1;
        this.setState(s1);
    }
    showHome = () => {
        let s1 = { ...this.state };
        s1.view = 0;
        this.setState(s1);
    }
    showReciveForm = () => {
        let s1 = { ...this.state };
        s1.view = 2;
        this.setState(s1);
    }

    editPerson = (index) => {
        let s1 = { ...this.state };
        s1.view = 1;
        s1.editPersonIndex = index
        this.setState(s1);
    }
  
    deletePerson = (index) => {
        let s1 = { ...this.state };

        s1.persons.splice(index, 1);
        this.setState(s1);
    }
    getQuantity=()=>{
        let s1 = { ...this.state };
        let quantity =0
        s1.products.map((e)=>{
            quantity+=e.quantity
        })
        return quantity
    }
    getValue=()=>{
        let s1 = { ...this.state };
        let value =0
        s1.products.map((e)=>{
            value+=e.quantity*e.price
        })
        return value
    }
    render() {
        let product = {
            code: "", price: 0, brand: "", category: "",
            specialOffer: false, limitedStock: false, quantity: 0
        };
        //let mymark={maths:"",science:"",english:"",hindi:""};
        let { products, editPersonIndex, view } = this.state;
        return (

            <div className="container">
                <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                    <a className="navbar-brand" to="/all/1">
                        ProdSys
                    </a>
                    <div className="" id="navbarSupportSystem">
                        <ul className="navbar-nav mr-auto">

                            <li className="nav-item">
                                <a className="nav-link" >
                                    Products <button className="btn btn-secondary rounded btn-sm">{products.length}</button>

                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link">
                                    Quantity <button className="btn btn-secondary rounded btn-sm">{this.getQuantity()}</button>

                                </a>
                            </li>

                            <li className="nav-item">
                                <a className="nav-link" >
                                    Value <button className="btn btn-secondary rounded btn-sm">{this.getValue()}</button>
                                </a>
                            </li>


                        </ul>

                    </div>
                </nav><br />

                {this.state.view == 1 ? < Input1 allProducts={products} product={editPersonIndex >= 0 ? products[editPersonIndex] : product} onSubmit={this.handlePerson}
                    edit={editPersonIndex >= 0} /> : null}
                {this.state.view == 2 ? < Recive products={products} onSubmit={this.handleStocks} /> : null}
                {view == 1 || view == 2 ? "" :
                    <div>
                        <div className="row">
                            {products.map((p1, index) => (
                                <div className="col-3 border bg-light text-center">
                                    <b>Code : {p1.code}</b><br /><br />
                                    Price : {p1.price}<br />
                                    Brand : {p1.brand}<br />
                                    Category : {p1.category}<br />
                                    Special Offer : {p1.specialOffer == true ? "yes" : "no"}<br />
                                    Limited Stock : {p1.limitedStock == true ? "yes" : "no"}<br />
                                    Quantity : {p1.quantity}<br />
                                    <button className="btn btn-warning btn-sm m-1 col-3" onClick={() => this.editPerson(index)}>Edit Details</button>
                                </div>
                            ))}
                        </div>
                        <button className="btn btn-primary col-3 m-2" onClick={() => this.showForm()}>Add New Product</button>
                        <button className="btn btn-primary col-3 m-2" onClick={() => this.showReciveForm()}>Recive stock</button>
                    </div>}
                {view == 1 || view == 2 ? <button className="btn btn-primary" onClick={() => this.showHome()}>Go back To home Page</button> : ""}
            </div>
        )

    }
}

export default MainComponent