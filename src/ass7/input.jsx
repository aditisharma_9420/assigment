import React, { Component } from "react";
class Input extends Component {
    state = {
        allProducts: this.props.allProducts,
        product: this.props.product,
        brands: [],
        errors: {},

        categories: ["Food", "Personal Care", "Apparel"],
        specials: ["Special Offer", "Limited stock"],
    };


    handleChange = (e) => {
        console.log(e.currentTarget);
        const { currentTarget: input } = e;
        let s1 = { ...this.state };
        if (input.type == 'text') {
            s1.product[input.name] = input.value
        }
        if (input.type == 'checkbox') {
            if (input.checked && input.value == 'Special Offer') {
                s1.product.specialOffer = true
            } else if (input.checked == false && input.value == 'Special Offer') {
                s1.product.specialOffer = false
            }
            if (input.checked && input.value == 'Limited stock') {
                s1.product.limitedStock = true
            } else if (input.checked == false && input.value == 'Limited stock') {
                s1.product.limitedStock = false
            }
        }
        if (input.type == 'radio') {
            if (input.checked) {
                console.log("categoryyy", input.value)
                let braArr = []
                s1.allProducts.map((e => {
                    if (e.category == input.value) {
                        let i = braArr.findIndex((e1) => {
                            return e1 == e.brand
                        })
                        if (i == -1) {
                            braArr.push(e.brand)
                        }
                    }
                }))
                s1.product.category = input.value
                s1.brands = braArr
            }
        }
        if (input.name == 'brand') {
            console.log('droppp==>', input.value)
            s1.product.brand = input.value
        }

        this.setState(s1);
    };
    validateForm=()=>{}
    handleSubmit = (e) => {
        e.preventDefault();
        console.log("handle submit", this.state.product);
        this.validateForm()
        this.props.onSubmit(this.state.product)

    }

    render() {
        let { code, price, category, brand, specialOffer, limitedStock, quantity } = this.state.product
        const { categories, specials, brands } = this.state;

        return (
            <div>
                <div className="container">
                    <h5>{this.props.edit ? "Edit details" : "Enter Details of person"}</h5>
                    <div className="form-group">
                        <label>Product Code</label>
                        <input type="text"
                            className="form-control"
                            id="code"
                            name="code"
                            value={code}
                            placeholder=""
                            onChange={this.handleChange} />
                    </div>
                    <br />

                    <div className="form-group">
                        <label>Price</label>
                        <input type="text"
                            className="form-control"
                            id="price"
                            name="price"
                            value={price}
                            placeholder=""
                            onChange={this.handleChange} />

                    </div>
                    <br />

                    <b>Category</b><br />
                    {categories.map((d1) => (<div className="form-check form-check-inline">
                        <input type="text"
                            className="form-check-input"
                            type="radio"
                            name="category"
                            value={d1}
                            checked={category === d1}
                            onChange={this.handleChange} />
                        <label className="form-check-label">{d1}</label>
                    </div>))}<br />

                    <div className="form-group"><br />
                        <select className="form-control"
                            name="brand" value={brand}
                            onChange={this.handleChange}
                        >
                            <option disabled value=""> Select the brand</option>
                            {brands.map((c1) => (
                                <option selected={c1 == brand}>{c1}</option>

                            ))}
                        </select>
                    </div>

                    <label className="form-check-label  font-weight-bold">
                        <b> Choose Other info about the Product</b>
                    </label>
                    {specials.map((t1) => (
                        <div className="form-check">
                            <input type="checkbox"
                                className="form-check-input"
                                checked={t1 == 'Special Offer' ? specialOffer : t1 == 'Limited stock' ? limitedStock : false}
                                name="special"
                                value={t1}
                                onChange={this.handleChange} />
                            <label className="form-check-label">{t1}</label>
                        </div>
                    ))}<br />
                </div><br />
                <button className="btn btn-primary" onClick={this.handleSubmit}>{this.props.edit ? "Edit Product" : "Add Product"}</button><br /><br />
            </div>

        )
    }
}
export default Input;