import React, { Component } from "react";
import DatePicker from "./datePicker";
class Recive extends Component {
    state = {

        products: this.props.products,
        currentProduct: '',
        stockQuantity: '',

        categories: ["Food", "Personal care", "Apparel"],
        specials: ["Special Offer", "Limited stock"],
    };




    handleSubmit = (e) => {
        // e.preventDefault();
        let s1 = { ...this.state }
        console.log("handle submit", s1.currentProduct);
        if(!s1.currentProduct){alert('Select a product first')}
        else if(!s1.stockQuantity){alert('Enter Stock Quantity')}
        else{
            this.props.onSubmit(s1.currentProduct, s1.stockQuantity)
        }
    }
    handleDD = (value) => {
        let s1 = { ...this.state }
        console.log('..', value)
        s1.currentProduct = value
        this.setState(s1)
    }
    render() {
        const { products, stockQuantity, currentProduct } = this.state;

        return (
            <div>
                <div className="container">
                    <h5>Select the product whose stocks have been recived</h5>

                    <div className="form-group">

                        <select className="form-control"
                            name="codev"
                            onChange={(value) => {
                                console.log(value.currentTarget.value)
                                this.handleDD(value.currentTarget.value)
                            }}
                        >
                            <option selected disabled value="">select the code</option>

                            {products.map((c1) => (
                                <option>{c1.code}</option>
                            ))}

                        </select>
                    </div>


                    <div className="form-group">
                        <label>Stocks recived</label>
                        <input type="number"
                            className="form-control"
                            id="stock"
                            name="stock"
                            value={stockQuantity}
                            placeholder=""
                            onChange={(value) => {
                                console.log(value.currentTarget.value)
                                this.setState({ stockQuantity: value.currentTarget.value })
                            }} />
                    </div>
                    <br />
                    <DatePicker></DatePicker>
                </div><br />
                <button className="btn btn-primary" onClick={this.handleSubmit}>Submit</button><br /><br />
            </div>
        )
    }
}
export default Recive;